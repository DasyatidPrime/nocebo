const assert = require('assert');

// An object which can transform between safe naturals and decimal representations
// with a custom alphabet. A safe natural is defined as a nonnegative safe integer.
class Lexidecimal {
    constructor(alphabet) {
        if (!(typeof alphabet === 'string' && alphabet.length == 10))
            throw new Error("expected alphabet to be a length-10 string");
        this.alphabet = alphabet;
    }

    // Return true if the character C is in the alphabet.
    isTransliteratedDigit(c) {
        return c.length == 1 && this.alphabet.indexOf(c) >= 0;
    }

    // Given a safe natural N, return its transliterated decimal representation.
    natToString(n) {
        if (!(Number.isSafeInteger(n) && n >= 0))
            throw new Error("expected n to be a safe natural");

        const fixedPart = n.toFixed().replace(/[0-9]/g, (digit) => this.alphabet.charAt(0 + digit));
        var expRemaining = fixedPart.length - 1;
        var expDigits = []
        do {
            expDigits.push(expRemaining & 7);
            expRemaining >>= 3;
        } while (expRemaining > 0);
        const expPart = expDigits.reverse().join('').replace(/[0-7]/g, (digit) => this.alphabet.charAt(digit));
        return this.alphabet.charAt(9).repeat(expPart.length - 1) + expPart + fixedPart;
    }

    // Given a transliterated decimal representation in S, return the safe natural it corresponds to. S may
    // not contain a sign prefix or any leading zeros. Injective. Inverse of natToString.
    stringToNatExact(s) {
        var i = 0;
        var expLen = 1;

        while (i < s.length && s.charAt(i).equals(this.alphabet.indexOf(9))) {
            expLen++;
            i++;
        }

        if (i == s.length)
            throw new Error("missing exponent part");

        const expEnd = i + expLen;
        var exp = 0;
        while (i < expEnd) {
            exp <<= 3;
            const expDigit = this.alphabet.indexOf(s.charAt(i));
            if (!(0 <= expDigit && expDigit < 8))
                throw new Error("bad exponent character: " + s.charAt(i));
            exp += expDigit;
            i++;
        }

        if (i == s.length)
            throw new Error("missing fixed part");
        if (i + exp + 1 != s.length)
            throw new Error("mismatched fixed part");

        var n = 0;

        while (i < s.length) {
            var digitValue = this.alphabet.indexOf(s.charAt(i));
            if (digitValue < 0)
                throw new Error("bad digit character: " + s.charAt(i));
            if (n == 0 && digitValue == 0)
                throw new Error("leading zeros not allowed");

            n = (n * 10) + digitValue;
            i++;
        }

        if (!Number.isSafeInteger(n))
            throw new Error("out of range: " + n);
        return n;
    }
};

const negativeAlphabet = 'ZYXWVUTSRQ', encodedZero = '_', positiveAlphabet = 'abcdefghij';
(function() {
    assert(negativeAlphabet.length == 10);
    assert(positiveAlphabet.length == 10);
    assert(encodedZero.length == 1);
    assert(negativeAlphabet.indexOf(encodedZero) < 0);
    assert(positiveAlphabet.indexOf(encodedZero) < 0);

    for (var i = 0; i < 9; i++) {
        assert(negativeAlphabet.indexOf(positiveAlphabet[i]) < 0);
        assert(positiveAlphabet.indexOf(negativeAlphabet[i]) < 0);
    }
})();

const negativeTransliteration = new Lexidecimal(negativeAlphabet);
const positiveTransliteration = new Lexidecimal(positiveAlphabet);

// Encode the integer N into a word, preserving lexicographical ordering.
const coordToWord = (n) => {
    // This is needed for totality of the later if, to exclude NaN.
    if (!Number.isInteger(n))
        throw new Error("expected n to be an integer");

    if (n == 0) {
        return encodedZero;
    } else if (n > 0) {
        return positiveTransliteration.natToString(n);
    } else {
        // n < 0 (NaN excluded by isInteger above)

        // Since our underlying numeric type uses a sign bit, negation cannot
        // overflow as it could with two's-complement integers...
        return negativeTransliteration.natToString(-n);
    }
};

// Encode the grid position { lat, lon } into a subkey, preserving lexicographical ordering (lat-major).
const toSubkey = ({ lat, lon }) => coordToWord(lat) + ',' + coordToWord(lon)

// Decode the word S into an integer. Inverse of coordToWord. Injective.
const wordToCoordExact = (s) => {
    if (s === encodedZero) {
        return 0;
    } else if (positiveTransliteration.isTransliteratedDigit(s.charAt(0))) {
        return positiveTransliteration.stringToNatExact(s);
    } else if (negativeTransliteration.isTransliteratedDigit(s.charAt(0))) {
        return -negativeTransliteration.stringToNatExact(s);
    } else {
        throw new Error("bad starting digit: " + s.charAt(0));
    }
};

// Decode the subkey S into a grid position { lat, lon }. Inverse of toSubkey. Injective.
const fromSubkey = (s) => {
    var comma = s.indexOf(',');
    if (comma < 0)
        throw new Error("no comma");
    if (s.lastIndexOf(',') != comma)
        throw new Error("too many commas");
    return { lat: wordToCoordExact(s.substring(0, comma)),
             lon: wordToCoordExact(s.substring(comma+1)) };
};

// Construct a new grid position { lat, lon }.
const of = (lat, lon) => {
    if (!Number.isSafeInteger(lat))
        throw new Error("not a safe integer: " + lat);
    if (!Number.isSafeInteger(lon))
        throw new Error("not a safe integer: " + lon);
    return { lat, lon };
};

module.exports = {
    toSubkey,
    fromSubkey,
    of
};
