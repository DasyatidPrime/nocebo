const express = require('express');
const app = express();

app.get('/', (req, resp, next) => {
    resp.statusCode = 200
    resp.setHeader('Content-Type', 'text/html')
    resp.end(`
<html>
<head>
<title>Twisty Little Passages</title>
</head>
<body>
<h1>Twisty Little Passages</h1>
<p>You are in a maze of twisty little JavaScript, all different.</p>
</body>
</html>
`);
});

app.listen(1080);
