const assert = require('chai').assert;
const gridPosition = require('../gridPosition');

describe('gridPosition', function() {
    describe('.of basics', function() {
        it('should return an object with lat/lon properties', function() {
            const object = gridPosition.of(3, 5);
            assert.typeOf(object, 'object');
            assert.equal(object.lat, 3);
            assert.equal(object.lon, 5);
        });

        it('should handle all sign combinations', function() {
            for (var lat = -5; lat <= 5; lat += 5) {
                for (var lon = -5; lon <= 5; lon += 5) {
                    const object = gridPosition.of(lat, lon);
                    assert.equal(object.lat, lat);
                    assert.equal(object.lon, lon);
                }
            }
        });

        it('should fail on non-integers', function() {
            assert.throws(() => gridPosition.of(3.5, 0));
            assert.throws(() => gridPosition.of(-5, 1.3));
        });

        it('should fail on non-numbers', function() {
            assert.throws(() => gridPosition.of('oh no'));
            assert.throws(() => gridPosition.of(-9, 'oh no'));
        });
    });

    describe('.toSubkey basics', function() {
        const subkeyOf = (lat, lon) => gridPosition.toSubkey(gridPosition.of(lat, lon));

        it('should return a string', function() {
            const subkey = subkeyOf(3, 5);
            assert.typeOf(subkey, 'string');
        });

        it('should return safe subkeys from all signs', function() {
            for (var lat = -5; lat <= 5; lat += 5) {
                for (var lon = -5; lon <= 5; lon += 5) {
                    // We haven't fully defined what a safe subkey is, but this is good enough for now.
                    assert.notMatch(subkeyOf(lat, lon), /\s|\//);
                }
            }
        });

        it('should preserve lexicographical ordering', function() {
            var lastLat, lastLon, lastSubkey;

            // Cover at least one extra digit position, all digits, and all sign combinations.
            for (var lat = -12; lat <= 12; lat++) {
                for (var lon = -12; lon <= 12; lon++) {
                    const subkey = subkeyOf(lat, lon);
                    // TODO: does chai assert support deferred construction of messages?
                    if (lastSubkey) {
                        assert(lastSubkey < subkey,
                               `${lastSubkey} (${lastLat}, ${lastLon}) < ${subkey} (${lat}, ${lon})`);
                    }

                    lastSubkey = subkey;
                    lastLat = lat;
                    lastLon = lon;
                }
            }
        });
    });
});
