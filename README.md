# Nocebo

>ED: (thoroughly frustrated) It's too late. You've awakened the gazebo.
>It catches you and eats you.
> –[“Eric and the Dread Gazebo”, tabletop RPG folktale](https://allthetropes.org/wiki/Eric_and_the_Dread_Gazebo)

This is a toy repo for me to play with learning Node in. Don't use it for anything serious. That means you, Eric.

It contains the following modules:

- `tlp`: Twisty Little Passages. Some data representation and unit tests. Will have: CRUD, DB stuff.
